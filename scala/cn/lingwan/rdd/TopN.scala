package cn.lingwan.rdd

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

// log analysis-----TopN(top N urls that are visited most frequently)
object TopN {
  def main(args: Array[String]): Unit = {
    val sparkConf: SparkConf = new SparkConf().setAppName("TopN").setMaster("local[2]")
    val sc = new SparkContext(sparkConf)
    sc.setLogLevel("warn")

    val data: RDD[String] = sc.textFile("E:\\data\\access.log")
    val url: RDD[String] = data.filter(x=>x.split(" ").length>10).map(x=>x.split(" ")(10))
    val urlAndOne: RDD[(String, Int)] = url.map(x=>(x,1))
    val result: RDD[(String, Int)] = urlAndOne.reduceByKey(_+_)
    val sortedRDD: RDD[(String, Int)] = result.sortBy(x=>x._2,false)
    val top5: Array[(String, Int)] = sortedRDD.take(5)
    top5.foreach(println)
  }

}
