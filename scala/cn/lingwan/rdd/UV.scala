package cn.lingwan.rdd

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.rdd.RDD

//TODO:log analysis------------UV
object UV {
  def main(args: Array[String]): Unit = {
    val sparkConf: SparkConf = new SparkConf().setAppName("UV").setMaster("local[2]")

    val sc = new SparkContext(sparkConf)
    sc.setLogLevel("warn")

    val data: RDD[String] = sc.textFile("E:\\data\\access.log")

      val ips: RDD[String] = data.map(x=>x.split(" ")(0))

      val distinctRDD: RDD[String] = ips.distinct()
	  
      val uv: Long = distinctRDD.count()
      println("UV:"+uv)

    sc.stop()
  }
}
