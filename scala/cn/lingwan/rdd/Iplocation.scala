package cn.lingwan.rdd

import org.apache.spark.broadcast.Broadcast
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Iplocation {
  //todo：ip转为long类型
  def ipToLong(ip: String): Long = {
    //todo：切分ip地址。
    val ipArray: Array[String] = ip.split("\\.")
    var ipNum=0L

    for(i <- ipArray){
      ipNum=i.toLong | ipNum <<8L
    }
    ipNum
  }

  //todo:通过二分查找法,获取ip在广播变量中的下标
  def binarySearch(ipNum: Long, broadCastValue: Array[(String, String, String, String)]): Int ={
    //todo:口诀：上下循环寻上下，左移右移寻中间
    //开始下标
    var start=0
    //结束下标
    var end=broadCastValue.length-1

    while(start<=end){
      val middle=(start+end)/2
      if(ipNum>=broadCastValue(middle)._1.toLong && ipNum<=broadCastValue(middle)._2.toLong){
        return middle
      }

      if(ipNum>broadCastValue(middle)._2.toLong){
        start=middle+1
      }

      if(ipNum<broadCastValue(middle)._1.toLong){
        end=middle-1
      }
    }

    -1
  }

  def main(args: Array[String]): Unit = {
    val sparkConf: SparkConf = new SparkConf().setAppName("Iplocation").setMaster("local[2]")
    val sc = new SparkContext(sparkConf)
    sc.setLogLevel("warn")

    val city_ip_rdd: RDD[(String, String, String, String)] = sc.textFile("E:\\data\\ip.txt").map(_.split("\\|")).map(x=>(x(2),x(3),x(x.length-2),x(x.length-1)))
    val cityIpBroadcast: Broadcast[Array[(String, String, String, String)]] = sc.broadcast(city_ip_rdd.collect())
    val ips: RDD[String] = sc.textFile("E:\\data\\20090121000132.394251.http.format").map(_.split("\\|")(1))
    val result: RDD[((String, String), Int)] = ips.mapPartitions(iter => {
      val broadcastValue: Array[(String, String, String, String)] = cityIpBroadcast.value
      iter.map(ip => {
        val ipNum: Long = ipToLong(ip)
        val index: Int = binarySearch(ipNum, broadcastValue)
        val value: (String, String, String, String) = broadcastValue(index)
        ((value._3, value._4), 1)
      })
    })

    val finalResult: RDD[((String, String), Int)] = result.reduceByKey(_+_)

    finalResult.foreach(println)
    sc.stop()

  }
}
