package cn.lingwan.rdd

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

//TODO:log analysis-------------PV
object PV {
  def main(args: Array[String]): Unit = {

     val sparkConf: SparkConf = new SparkConf().setAppName("PV").setMaster("local[2]")

     val sc = new SparkContext(sparkConf)
     sc.setLogLevel("warn")

     val data: RDD[String] = sc.textFile("E:\\data\\access.log")

     val pv: Long = data.count()
     println("PV:"+pv)

     sc.stop()
  }

}
