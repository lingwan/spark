package cn.lingwan.wordCount

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object WordCount {
  def main(args: Array[String]): Unit = {
    val sparkConf: SparkConf = new SparkConf().setAppName("WordCount").setMaster("local[2]")
    val sc = new SparkContext(sparkConf)
    sc.setLogLevel("warn")
    val data: RDD[String] = sc.textFile("E:\\test\\words.txt")
    val words: RDD[String] = data.flatMap(_.split(" "))
    val wordAndOne: RDD[(String, Int)] = words.map((_,1))
    val result: RDD[(String, Int)] = wordAndOne.reduceByKey(_+_)
    val sortedRDD: RDD[(String, Int)] = result.sortBy(_._2)
    val finalResult: Array[(String, Int)] = sortedRDD.collect()
    finalResult.foreach(println)
    sc.stop()
  }
}
