package cn.lingwan.sparksql

import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.types.{IntegerType, StringType, StructField, StructType}
import org.apache.spark.sql.{DataFrame, Row, SparkSession}

// todo: Using StructType to define schema, rdd is changed to dataFrame
object SparksqlSchema {
  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession.builder().appName("SparksqlSchema").master("local[2]").getOrCreate()
    val sc: SparkContext = spark.sparkContext
    sc.setLogLevel("warn")

    val rdd1: RDD[Array[String]] = sc.textFile("E:\\data\\person.txt").map(_.split(" "))

    val rowRDD: RDD[Row] = rdd1.map(x=>Row(x(0).toInt,x(1),x(2).toInt))

    val schema =
      StructType(
        StructField("id", IntegerType, true) ::
          StructField("name", StringType, false) ::
          StructField("age", IntegerType, false) :: Nil)

    val dataFrame: DataFrame = spark.createDataFrame(rowRDD,schema)

    dataFrame.printSchema()
    dataFrame.show()

    dataFrame.createTempView("t_person")
    spark.sql("select * from t_person where age>25").show()

    sc.stop()
    spark.stop()
  }
}
