package cn.lingwan.sparksql

import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{Column, DataFrame, SparkSession}



//todo: rdd is changed to dataFrame by reflection (define case class)

case class Person(id:Int,name:String,age:Int)
object CaseClassSchema {
  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession.builder().appName("CaseClassSchema").master("local[2]").getOrCreate()
    val sc: SparkContext = spark.sparkContext
    sc.setLogLevel("warn")
    val rdd1: RDD[Array[String]] = sc.textFile("E:\\data\\person.txt").map(_.split(" "))
    val personRDD: RDD[Person] = rdd1.map(x=>Person(x(0).toInt,x(1),x(2).toInt))
    import spark.implicits._
    val personDF: DataFrame = personRDD.toDF
    personDF.printSchema()

    //dsl style start
    personDF.show()

    personDF.show(1)
    println(personDF.first())
    println(personDF.head())

    personDF.select("name").show()
    personDF.select($"name").show()
    personDF.select(new Column("name")).show()
    personDF.select("name","age","id").show()

    personDF.select($"age",$"age"+1).show()

    personDF.filter($"age">30).show()
    println(personDF.filter($"age">30).count())
    personDF.groupBy($"age").count().show()
    // dsl style end

    // sql style start
    personDF.createTempView("person")
    spark.sql("select * from person").show()
    spark.sql("select count(*) from person").show()
    spark.sql("select * from person where age>30").show()
    spark.sql("select * from person order by age desc").show()
    // sql style end

    sc.stop()
    spark.stop()

  }
}
